package com.asklytics.enroll.exception;

public class EnrollmentException extends Exception {
	private static final long serialVersionUID = 1L;

	public EnrollmentException(String message) {
		super(message);
	}

	public EnrollmentException(Throwable t) {
		super(t);
	}

	public EnrollmentException(String message, Throwable t) {
		super(message, t);
	}

}
