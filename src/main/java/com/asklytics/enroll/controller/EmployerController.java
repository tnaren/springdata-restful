package com.asklytics.enroll.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asklytics.enroll.exception.EnrollmentException;
import com.asklytics.enroll.service.EmployeeService;
import com.asklytics.enroll.service.EmployerService;
import com.asklytics.enroll.vo.EmployerVo;

@RestController
@RequestMapping("/employer")
public class EmployerController {
	final static Logger logger = Logger.getLogger(EmployerController.class);

	@Autowired
	private EmployerService employerService;

	@PostMapping
	public ResponseEntity<EmployerVo> addEmployer(@RequestBody EmployerVo employer) throws EnrollmentException {
		EmployerVo employerVo = employerService.save(employer);
		logger.debug("Added:: " + employerVo);
		return new ResponseEntity<EmployerVo>(employerVo, HttpStatus.CREATED);
	}

	@GetMapping("/list")
	public ResponseEntity<List<EmployerVo>> getEmployers() throws EnrollmentException {
		List<EmployerVo> employers = employerService.getAll();
		return new ResponseEntity<List<EmployerVo>>(employers, HttpStatus.OK);
	}

	@GetMapping("/list/{id}")
	public ResponseEntity<EmployerVo> getEmployer(@PathVariable("id") Long id) throws EnrollmentException {
		EmployerVo employer = employerService.getById(id);
		return new ResponseEntity<EmployerVo>(employer, HttpStatus.OK);
	}

	@DeleteMapping("/remove/{id}")
	public ResponseEntity<String> removeEmployer(@PathVariable("id") Long id) throws EnrollmentException {

		EmployerVo employer = employerService.getById(id);
		if (null != employer) {
			boolean success = employerService.delete(id);

			if (success) {
				return new ResponseEntity("Sucess", HttpStatus.OK);
			} else {
				return new ResponseEntity("Unable to remove", HttpStatus.EXPECTATION_FAILED);
			}
		} else {
			return new ResponseEntity("Resource Unavailable", HttpStatus.NOT_FOUND);
		}
	}


	@Transactional
	@PutMapping("/update")
	public ResponseEntity<EmployerVo> updateEmployer(@RequestBody EmployerVo employer) throws EnrollmentException  {
		
		if(StringUtils.isEmpty(employer.getEmployerName().trim())){
			throw new BadRequestException("In Valid Request" + employer.getEmployerName());
		}
		
		if( null== employer.getEmployerId() || employer.getEmployerId()<=0){
			throw new BadRequestException("In Valid Request" + employer.getEmployerId());
		}
		
		EmployerVo employerVo = employerService.update(employer);
		logger.debug("Added:: " + employerVo);
		return new ResponseEntity<EmployerVo>(employerVo, HttpStatus.OK);
	}
}
