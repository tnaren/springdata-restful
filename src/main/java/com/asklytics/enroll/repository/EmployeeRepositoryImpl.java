package com.asklytics.enroll.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.asklytics.enroll.entity.Employee;


@Repository
@Transactional
public class EmployeeRepositoryImpl implements EmployeeCustomRepository {
	
	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public Employee findByEmployerId(Long employerId) {
		Query query = entityManager.createNativeQuery("select * from employee emp " +
                "where emp.employer_id = ? " ,Employee.class);
        query.setParameter(1, employerId);
        
        return (Employee) query.getSingleResult();
	}

	@Override
	public boolean deleteByEmployerId(Long employerId) {
		Query query = entityManager.createNativeQuery("delete from employee emp " +
                "where emp.employer_id = ? " );
       int count = query.setParameter(1, employerId).executeUpdate();
        if(count > 0){
        	return true;
        }
        return false;
    }

	@Override
	public boolean deleteByEmployeeId(Long employeeId) {
		Query query = entityManager.createNativeQuery("delete from employee emp " +
                "where emp.id = ? " );
       int count = query.setParameter(1, employeeId).executeUpdate();
        if(count > 0){
        	return true;
        }
        return false;
	}

}
