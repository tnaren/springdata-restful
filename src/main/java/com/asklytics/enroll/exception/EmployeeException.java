package com.asklytics.enroll.exception;

public class EmployeeException extends EnrollmentException {
	private static final long serialVersionUID = 1L;

	public EmployeeException(String message) {
		super(message);
	}
}
