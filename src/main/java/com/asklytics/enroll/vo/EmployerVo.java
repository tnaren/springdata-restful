package com.asklytics.enroll.vo;

public class EmployerVo extends BaseRequest {

	private static final long serialVersionUID = 1L;

	private Long employerId;

	private String employerName;

	public Long getEmployerId() {
		return employerId;
	}

	public void setEmployerId(Long employerId) {
		this.employerId = employerId;
	}

	public String getEmployerName() {
		return employerName;
	}

	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}

}
