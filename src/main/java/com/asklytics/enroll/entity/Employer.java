package com.asklytics.enroll.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "employer")
public class Employer extends BaseEntity {

	private static final long serialVersionUID = 4910225916550731446L;

	public Employer() {
	}

	@Column(name = "employername", length = 50, nullable = false, unique = true)
	private String employerName;

	public String getEmployerName() {
		return employerName;
	}

	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}

}
