package com.asklytics.enroll.test;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Before;
import org.junit.Test;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.asklytics.enroll.exception.BadRequestException;
import com.asklytics.enroll.exception.EmployerException;
import com.asklytics.enroll.exception.ResourceNotFoundException;
import com.asklytics.enroll.vo.EmployerVo;

import io.restassured.response.Response;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class EmployerTest {

	@Value("${local.server.port}")
	int port;

	@Before
	public void setUp() throws Exception {

		baseURI = "http://localhost";
		this.port = port;
	}

	String emplyr1 = "{\"employerName\":\"testEmployer\"}";
	String emplyr2 = "{\"employerName\":\"testEmployer1\"}";
	String emplyr3 = "{\"employerName\":\"testEmployer2\"}";

	String emplyr4 = "{\"employerName\":\"testEmployer3\"}";
	String emplyr5 = "{\"employerName\":\"testEmployer4\"}";
	String emplyr6 = "{\"employerName\":\"testEmployer5\"}";

	String exception1 = "{\"httpStatus\":\"EXPECTATION_FAILED\",\"errorMessage\":\"testEmployer  already exists\",\"url\":\"http://localhost:8080/enrollment/employer\"}";

	@Test
	public void getEmployerTestSuccess() {

		given().contentType("application/json").body(emplyr1).when().post("/enrollment/employer").then()
				.statusCode(201);
	}

	@Test
	public void getEmployerTest() {

		EmployerVo e = given().contentType("application/json").body(emplyr2).when().post("/enrollment/employer").then()
				.extract().as(EmployerVo.class);

		assertNotNull("After Insert Employer Object Should not be null", e);
	}

	@Test
	public void shouldReturnForTheId() {
		EmployerVo emplyr = given().contentType("application/json").body(emplyr3).when().post("/enrollment/employer")
				.then().extract().as(EmployerVo.class);

		EmployerVo emplyr1 = given().get("/enrollment/employer/list/1").then().contentType("").extract().response()
				.getBody().as(EmployerVo.class);

		assertEquals(emplyr1.getEmployerName(), "testEmployer2");
	}

	@Test
	public void getResponseAsBadRequest() throws BadRequestException {

		given().get("/enrollment/employer/list/0").then().statusCode(400);
	}

	@Test
	public void get404Exception() throws ResourceNotFoundException {

		given().get("/enrollment/employer/list/10").then().statusCode(404);
	}

	@Test
	public void getList() {
		given().contentType("application/json").body(emplyr4).when().post("/enrollment/employer");

		given().contentType("application/json").body(emplyr5).when().post("/enrollment/employer");

		given().contentType("application/json").body(emplyr6).when().post("/enrollment/employer");

		EmployerVo[] emplyrArr = given().get("/enrollment/employer/list").as(EmployerVo[].class);
		// prviously saved 3 records
		assertEquals(5, emplyrArr.length);
	}

	
	
	
	@Test
	public void getEmplyrException() throws EmployerException {
		Response response = given().contentType("application/json").body(emplyr1).when().post("/enrollment/employer")
				.then().statusCode(417).extract().response();
		assertEquals(exception1, response.asString());
	}
	
	
	
	//remove Test case is execting before before getList method
	@Test
	public void removeEmployer() throws EmployerException {
		String response = given().delete("/enrollment/employer/remove/1").then().contentType("").extract().response().asString();
		assertEquals("Resource Sucessfully removed", response);	
		
		System.out.println("respone" + response);
		EmployerVo[] emplyrArr = given().get("/enrollment/employer/list").as(EmployerVo[].class);
		
		assertEquals(1, emplyrArr.length);		
		
	}
	
	
	
	

}
