package com.asklytics.enroll.test;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.asklytics.enroll.exception.BadRequestException;
import com.asklytics.enroll.exception.ResourceNotFoundException;
import com.asklytics.enroll.vo.EmployeeVo;
import com.asklytics.enroll.vo.EmployerVo;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class EmployeeTest {

	@Value("${local.server.port}")
	int port;

	@Before
	public void setUp() throws Exception {

		baseURI = "http://localhost";
		this.port = port;
	}

	String emplyr1 = "{\"employerName\":\"testEmployer0\"}";
	String emplyr2 = "{\"employerName\":\"testEmployer01\"}";
	String emplyr3 = "{\"employerName\":\"testEmployer02\"}";

	String emplyr4 = "{\"employerName\":\"testEmployer03\"}";
	String emplyr5 = "{\"employerName\":\"testEmployer04\"}";
	String emplyr6 = "{\"employerName\":\"testEmployer05\"}";

	private EmployeeVo getEmploye() {
		EmployeeVo emp = new EmployeeVo();
		EmployerVo emplyr = new EmployerVo();
		emp.setDesignation("Developer");
		emp.setFirstName("employ");
		emp.setLastName("N");
		emp.setSalary(new BigDecimal("20000.00"));
		emplyr.setEmployerName("testEmployer0");
		emp.setEmployer(emplyr);
		return emp;
	}

	private EmployeeVo getEmploye1() {
		EmployeeVo emp = new EmployeeVo();
		EmployerVo emplyr = new EmployerVo();
		emp.setDesignation("Lead");
		emp.setFirstName("employ1");
		emp.setLastName("N");
		emp.setSalary(new BigDecimal("50000.00"));
		emplyr.setEmployerName("testEmployer0");
		emp.setEmployer(emplyr);
		return emp;
	}

	private EmployeeVo getEmploye2() {
		EmployeeVo emp = new EmployeeVo();
		EmployerVo emplyr = new EmployerVo();
		emp.setDesignation("eveloper");
		emp.setFirstName("employ2");
		emp.setLastName("N");
		emp.setSalary(new BigDecimal("20000.00"));
		emplyr.setEmployerName("testEmployer01");
		emp.setEmployer(emplyr);
		return emp;
	}

	@Test
	public void shouldReturnForTheId() {

		given().contentType("application/json").body(emplyr1).when().post("/enrollment/employer").then()
				.statusCode(201);

		given().contentType("application/json").body(getEmploye()).when().post("/enrollment/employee").then()
				.statusCode(201);

		EmployeeVo emply1 = given().get("/enrollment/employee/list/1").then().contentType("").extract().response()
				.getBody().as(EmployeeVo.class);

		assertEquals(emply1.getEmployer().getEmployerName(), "testEmployer0");
		assertEquals(emply1.getFirstName(), "employ");
	}

	@Test
	public void getEmployeList() {
		given().contentType("application/json").body(getEmploye1()).when().post("/enrollment/employee").then()
				.statusCode(201);

		EmployeeVo[] emplArr = given().get("/enrollment/employee/list").as(EmployeeVo[].class);
		assertEquals(4, emplArr.length);

	}
	
	@Test
	public void removeEmployee() {
		
		given().contentType("application/json").body(getEmploye1()).when().post("/enrollment/employee").then()
		.statusCode(201);
		
		given().contentType("application/json").body(emplyr2).when().post("/enrollment/employer").then()
		.statusCode(201);
		
		given().contentType("application/json").body(getEmploye2()).when().post("/enrollment/employee").then()
		.statusCode(201);
		
		given().contentType("application/json").when().delete("/enrollment/employee/2").then()
				.and().extract().jsonPath();
		
		EmployeeVo[] emplArr = given().get("/enrollment/employee/list").as(EmployeeVo[].class);
		
		assertEquals(3, emplArr.length);
		

	}
	
	

	@Test
	public void getResponseAsBadRequest() throws BadRequestException {

		given().get("/enrollment/employee/list/0").then().statusCode(400);
	}

	@Test
	public void get404Exception() throws ResourceNotFoundException {

		given().get("/enrollment/employee/list/10").then().statusCode(404);
	}

}
