package com.asklytics.enroll.exception;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
	final static Logger logger = Logger.getLogger(GlobalExceptionHandler.class);

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ApplicationErrorResponse> unhandledException(HttpServletRequest request, Exception e) {
		ApplicationErrorResponse response = new ApplicationErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR,
				e.getMessage(), request.getRequestURL().toString());
		logger.error(e.getMessage(), e);
		return new ResponseEntity<>(response, response.getHttpStatus());
	}

	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<ApplicationErrorResponse> runtimeException(HttpServletRequest request, RuntimeException e) {
		ApplicationErrorResponse response = new ApplicationErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR,
				e.getMessage(), request.getRequestURL().toString());
		logger.error(e.getMessage(), e);
		return new ResponseEntity<>(response, response.getHttpStatus());
	}

	@ExceptionHandler(EnrollmentException.class)
	public ResponseEntity<ApplicationErrorResponse> applicationException(HttpServletRequest request,
			EnrollmentException e) {
		ApplicationErrorResponse response = new ApplicationErrorResponse(HttpStatus.EXPECTATION_FAILED, e.getMessage(),
				request.getRequestURL().toString());
		logger.error(e.getMessage(), e);
		return new ResponseEntity<>(response, response.getHttpStatus());
	}

}
