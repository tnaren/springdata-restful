package com.asklytics.enroll.vo;

import java.math.BigDecimal;

public class EmployeeVo extends BaseRequest {
	private static final long serialVersionUID = 1L;

	private String firstName;

	private String lastName;

	private String designation;

	private BigDecimal salary;

	private EmployerVo employer;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public BigDecimal getSalary() {
		return salary;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}

	public EmployerVo getEmployer() {
		return employer;
	}

	public void setEmployer(EmployerVo employer) {
		this.employer = employer;
	}
}
