package com.asklytics.enroll.test;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import io.restassured.path.json.JsonPath;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class HealthTest {
	@Value("${local.server.port}")
	int port;

	@Before
	public void setUp() throws Exception {

		baseURI = "http://localhost";
		this.port = port;
	}

	@Test
	public void getHealthTestSuccess() {

		JsonPath res =	given().get("/enrollment/health").then().statusCode(200).extract().response().jsonPath();
		String healthCheck = res.get("status");
		
		assertEquals(healthCheck,"UP");
	}
	
	
}
