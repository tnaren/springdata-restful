package com.asklytics.enroll.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asklytics.enroll.entity.Employee;
import com.asklytics.enroll.entity.Employer;
import com.asklytics.enroll.exception.EmployerException;
import com.asklytics.enroll.repository.EmployeeRepository;
import com.asklytics.enroll.repository.EmployerRepository;
import com.asklytics.enroll.service.EmployerService;
import com.asklytics.enroll.vo.EmployerVo;

@Service("employerService")
public class EmployerServiceImpl implements EmployerService {

	final static Logger logger = Logger.getLogger(EmployerServiceImpl.class);

	@Autowired
	private EmployerRepository employerRepository;
	
	@Autowired
	private EmployeeRepository employeeRepository;

	@Override
	public EmployerVo save(EmployerVo entity) throws EmployerException {
		try {
			Employer employer = employerRepository.findByEmployerName(entity.getEmployerName());
			if (employer != null) {
				throw new EmployerException(entity.getEmployerName() + "  already exists");
			}
			employer = employerRepository.save(prepareEmployer(entity));
			return prepareEmployerVo(employer);

		} catch (EmployerException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new EmployerException(e.getMessage());
		}

	}

	@Override
	public EmployerVo getById(Long id)throws EmployerException {
		try {
			Employer emp  = employerRepository.findOne(id);
			return prepareEmployerVo(emp);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new EmployerException(e.getMessage());
		}
	}

	@Override
	public List<EmployerVo> getAll() throws EmployerException {
		try {
			List<Employer> listOfEmployers = employerRepository.findAll();
			return prepareListOfEmployerVo(listOfEmployers);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new EmployerException(e.getMessage());
		}
	}

	@Override
	public boolean delete(Long employerId) {
		Employee employee = employeeRepository.findByEmployerId(employerId);
		
		if( null != employee ){
			employeeRepository.deleteByEmployerId(employerId);
			employerRepository.delete(employerId);
			
			return true;
		}
		
		return false;

	}

	@Override
	public EmployerVo update(EmployerVo employer) throws EmployerException {
		try {
			Employer existEmplyr = employerRepository.findOne(employer.getEmployerId());
			if (existEmplyr != null) {
				 existEmplyr = employerRepository.save(prepareUpdateEmployer(employer,existEmplyr.getId()));
			}
			
			return prepareEmployerVo(existEmplyr);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new EmployerException(e.getMessage());
		}
	}

	private Employer prepareEmployer(EmployerVo employerVo) {
		Employer employer = null;
		if (employerVo != null) {
			employer = new Employer();
			employer.setCreatedDate(new Date());
			employer.setEmployerName(employerVo.getEmployerName());
			employer.setCreatedBy(employerVo.getUserName());
		}
		return employer;
	}

	private EmployerVo prepareEmployerVo(Employer employer) {
		EmployerVo empVo = null;
		if (employer != null) {
			empVo = new EmployerVo();
			empVo.setEmployerName(employer.getEmployerName());
			empVo.setEmployerId(employer.getId());
		}
		return empVo;
	}

	private Employer prepareUpdateEmployer(EmployerVo employerVo,Long id) {
		Employer employer = null;
		if (employerVo != null) {
			employer = new Employer();
			employer.setModifiedDate(new Date());
			employer.setEmployerName(employerVo.getEmployerName());
			employer.setId(id);
			employer.setModifitedBy(employerVo.getUserName());
			
		}
		return employer;
	}


	private List<EmployerVo> prepareListOfEmployerVo(List<Employer> employers) {
		List<EmployerVo> employersVo = null;
		if (employers != null) {
			employersVo = new ArrayList<>();
			for (Employer employer : employers) {
				employersVo.add(prepareEmployerVo(employer));
			}
		}
		return employersVo;
	}

}
