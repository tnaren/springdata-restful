package com.asklytics.enroll.service;


import java.io.Serializable;
import java.util.List;

import com.asklytics.enroll.exception.EnrollmentException;
import com.asklytics.enroll.vo.BaseRequest;

public interface CrudService<E extends BaseRequest, ID extends Serializable> {

	E save(E entity) throws EnrollmentException;

	E getById(ID id) throws EnrollmentException;

	List<E> getAll() throws EnrollmentException;

	boolean delete(ID id) throws EnrollmentException;
	
	E update(E entity) throws EnrollmentException;
}
