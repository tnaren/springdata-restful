
package com.asklytics.enroll.exception;

import java.io.Serializable;

import org.springframework.http.HttpStatus;

public class ApplicationErrorResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private HttpStatus httpStatus;

	private String errorMessage;

	private String url;

	public ApplicationErrorResponse() {

	}

	public ApplicationErrorResponse(HttpStatus httpStatus, String errorMessage) {
		this.httpStatus = httpStatus;
		this.errorMessage = errorMessage;
	}

	public ApplicationErrorResponse(HttpStatus httpStatus, String errorMessage, String url) {
		this.httpStatus = httpStatus;
		this.errorMessage = errorMessage;
		this.url = url;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
