package com.asklytics.enroll.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.asklytics.enroll.entity.Employer;

@Repository
public interface EmployerRepository extends JpaRepository<Employer, Long> {
	
	public Employer findByEmployerName(String employerName);
}
