package com.asklytics.enroll.repository;

import com.asklytics.enroll.entity.Employee;

public interface EmployeeCustomRepository {

	Employee findByEmployerId(Long employerId);

	boolean deleteByEmployerId(Long employerId);
	
	boolean deleteByEmployeeId(Long employeeId);


}
