package com.asklytics.enroll.service;

import com.asklytics.enroll.vo.EmployeeVo;


public interface EmployeeService extends CrudService<EmployeeVo, Long> {

}
