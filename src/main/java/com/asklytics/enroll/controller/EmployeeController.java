package com.asklytics.enroll.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asklytics.enroll.exception.EnrollmentException;
import com.asklytics.enroll.service.EmployeeService;
import com.asklytics.enroll.vo.EmployeeVo;
import com.asklytics.enroll.vo.EmployerVo;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	final static Logger logger = Logger.getLogger(EmployeeController.class);

	@Autowired
	private EmployeeService employeeService;

	@PostMapping
	public ResponseEntity<EmployeeVo> addEmployee(@RequestBody EmployeeVo employee) throws EnrollmentException  {
		EmployeeVo employeeVo = employeeService.save(employee);
		logger.debug("Added:: " + employee);
		return new ResponseEntity<EmployeeVo>(employeeVo, HttpStatus.CREATED);
	}
	
	@GetMapping("/list")
	public ResponseEntity<List<EmployeeVo>> getEmployers() throws EnrollmentException  {
		List<EmployeeVo> employers = employeeService.getAll();
		return new ResponseEntity<List<EmployeeVo>>(employers, HttpStatus.OK);
	}
	
	@GetMapping("/list/{id}")
	public ResponseEntity<EmployeeVo> getEmployee(@PathVariable("id") Long id) throws EnrollmentException  {
		EmployeeVo employee = employeeService.getById(id);
		return new ResponseEntity<EmployeeVo>(employee, HttpStatus.OK);
	}
		
	
	@DeleteMapping("/remove/{id}")
	public ResponseEntity<String> removeEmployer(@PathVariable("id") Long id) throws EnrollmentException {

		EmployeeVo employer = employeeService.getById(id);
		if (null != employer) {
			boolean success = employeeService.delete(id);

			if (success) {
				return new ResponseEntity("Sucess", HttpStatus.OK);
			} else {
				return new ResponseEntity("Unable to remove", HttpStatus.EXPECTATION_FAILED);
			}
		} else {
			return new ResponseEntity("Resource Unavailable", HttpStatus.NOT_FOUND);
		}
	}

	@Transactional
	@PutMapping("/update")
	public ResponseEntity<EmployeeVo> updateEmployee(@RequestBody EmployeeVo employee) throws EnrollmentException  {
		
		if(null == employee  || !ValidationUtil.validateEmployee(employee) ){
			throw new BadRequestException("EmployeeFirstName/Salary/EmployerName cannot be empty or zero");
		}
		
		EmployeeVo employeeVo = employeeService.update(employee);
		logger.debug("Added:: " + employee);
		if(null != employeeVo){
			return new ResponseEntity<EmployeeVo>(employeeVo, HttpStatus.CREATED);
		}else{
			throw new ResourceNotFoundException("Resource Not Found " + employee.getEmployeeId()) ;
		}
		
	}
	

}
