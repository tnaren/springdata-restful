package com.asklytics.enroll.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asklytics.enroll.entity.Employee;
import com.asklytics.enroll.entity.Employer;
import com.asklytics.enroll.exception.EmployeeException;
import com.asklytics.enroll.repository.EmployeeRepository;
import com.asklytics.enroll.repository.EmployerRepository;
import com.asklytics.enroll.service.EmployeeService;
import com.asklytics.enroll.vo.EmployeeVo;
import com.asklytics.enroll.vo.EmployerVo;

@Service("employeeService")
public class EmployeeServiceImpl implements EmployeeService {
	
	final static Logger logger = Logger.getLogger(EmployeeServiceImpl.class);

	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private EmployerRepository employerRepository;
	

	@Override
	public EmployeeVo save(EmployeeVo entity)throws EmployeeException {
		try {
			Employer employer = employerRepository.findByEmployerName(entity.getEmployer().getEmployerName());
			if (null == employer ) {
				throw new EmployeeException(entity.getEmployer().getEmployerName() + "  does not exists");
			}
			Employee employee = employeeRepository.save(prepareEmployee(entity, employer));
			return prepareEmployeeVo(employee);

		} catch (EmployeeException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new EmployeeException(e.getMessage());
		}
	}

	@Override
	public EmployeeVo getById(Long id) throws EmployeeException {
		try {
			Employee emp  = employeeRepository.findOne(id);
			return prepareEmployeeVo(emp);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new EmployeeException(e.getMessage());
		}
	}

	@Override
	public List<EmployeeVo> getAll() throws EmployeeException {
		try {
			List<Employee> listOfEmployes = employeeRepository.findAll();
			return prepareListOfEmployeeVo(listOfEmployes);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new EmployeeException(e.getMessage());
		}
	}

	@Override
	public boolean delete(Long employeeId) {
			 return	employeeRepository.deleteByEmployeeId(employeeId);
	}
	
	
	@Override
	public EmployeeVo update(EmployeeVo entity) throws EnrollmentException {
		try {
			Employer employer = employerRepository.findByEmployerName(entity.getEmployer().getEmployerName());
			if (null == employer ) {
				throw new EmployeeException(entity.getEmployer().getEmployerName() + "  does not exists");
			}else{
				Employee existsEmployee = employeeRepository.findOne(entity.getEmployeeId());
				
				existsEmployee = null != existsEmployee ? employeeRepository.save(prepareUpdateEmployee(entity, employer,existsEmployee.getId())) : null;
				
				return prepareEmployeeVo(existsEmployee);
			}
		
		} catch (EmployeeException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new EmployeeException(e.getMessage());
		}
	}


	private Employee prepareEmployee(EmployeeVo employerVo, Employer employer) {
		Employee employee = null;
		if (employerVo != null) {
			employee = new Employee();
			employee.setCreatedDate(new Date());
			employee.setDesignation(employerVo.getDesignation());
			employee.setFirstName(employerVo.getFirstName());
			employee.setLastName(employerVo.getLastName());
			employee.setSalary(employerVo.getSalary());
			//BeanUtils.copyProperties(employee, employerVo);
			employee.setEmployer(employer);
		}
		return employee;
	}
	
	private Employee prepareUpdateEmployee(EmployeeVo employeeVo, Employer employer, Long employeId) {
		Employee employee = null;
		if (employeeVo != null) {
			employee = new Employee();
			employee.setModifiedDate(new Date());
			employee.setDesignation(employeeVo.getDesignation());
			employee.setFirstName(employeeVo.getFirstName());
			employee.setLastName(employeeVo.getLastName());
			employee.setSalary(employeeVo.getSalary());
			employee.setModifitedBy(employeeVo.getUserName());
			
			employee.setId(employeId);
			
			employee.setEmployer(employer);
		}
		return employee;
	}

	private EmployeeVo prepareEmployeeVo(Employee employee) {
		EmployeeVo emplyVo = null;
		if (employee != null) {
			emplyVo = new EmployeeVo();
			//BeanUtils.copyProperties(emplyVo, employee);
			emplyVo.setDesignation(employee.getDesignation());
			emplyVo.setFirstName(employee.getFirstName());
			emplyVo.setLastName(employee.getLastName());
			emplyVo.setSalary(employee.getSalary());
			emplyVo.setEmployer(prepareEmployerVo(employee.getEmployer()));
		}
		return emplyVo;
	}
	
	private EmployerVo prepareEmployerVo(Employer employer) {
		EmployerVo employerVo = null;
		if(employer != null) {
			employerVo = new  EmployerVo();
			employerVo.setEmployerId(employer.getId());
			employerVo.setEmployerName(employer.getEmployerName());
		}
		return employerVo;
	}

	private List<EmployeeVo> prepareListOfEmployeeVo(List<Employee> employes) {
		List<EmployeeVo> employesVo = null;
		if (employes != null) {
			employesVo = new ArrayList<>();
			for (Employee employer : employes) {
				employesVo.add(prepareEmployeeVo(employer));
			}
		}
		return employesVo;

	}
}
