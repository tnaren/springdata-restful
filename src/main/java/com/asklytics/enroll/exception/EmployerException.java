package com.asklytics.enroll.exception;

public class EmployerException extends EnrollmentException {
	private static final long serialVersionUID = 1L;

	public EmployerException(String message) {
		super(message);
	}
}
